﻿using System;

namespace ComputedEvolution
{
    public class entity
    {
        public int Size;
        public int Age;
        public int Death;

        public entity(int size, int age, int death)
        {
            this.Size = size;
            this.Age = age;
            this.Death = death;
        }

        public void writeSize()
        {
            Console.Write("\"" + Size.ToString() + "," + Age.ToString() +  "\", ");
        }
    }
}

