﻿using System;
using System.Collections.Generic;

namespace ComputedEvolution
{
    class MainClass
    {
        public static int generations = 100;

        public static int popSize = 100;
        public static List<entity> adultPopulation = new List<entity>();
        public static List<entity> babyPopulation = new List<entity>();
        public static List<entity> breedingSet = new List<entity>();

        public static Random rand = new Random();
        public static int minEntitySize = 1;
        public static int maxEntitySize = 50;

        public static int adultAge = 3;
        public static int deathAge = 10;

        public static int breedingPercentage = 90;
        public static int minOffspring = 1;
        public static int maxOffspring = 5;

        public static void Main(string[] args)
        {
            // Initialise the main population to start the 
            // simulation with
            populateList(popSize);

            // Loop through the entire evolutionary logic
            // each loop is 1 year, or 1 generation
            for (int i = 0; i < generations; i++)
            {
                // Kill off adults over a certain age 
                foreach (entity e in adultPopulation.ToArray())
                {
                    if (e.Age >= deathAge)
                    {
                        adultPopulation.Remove(e);
                    }
                }

                // Clear the screen for later display
                Console.Clear();

                // Add any babies of a high enough age
                // to the adult population
                foreach (entity e in babyPopulation.ToArray())
                {
                    if (e.Age >= adultAge)
                    {
                        adultPopulation.Add(e);
                        babyPopulation.Remove(e);
                    }
                }

                // Pick 10% of the adults to breed
                int breederCount = (int)(adultPopulation.Count * 0.10f);
                for (int j = 0; j < breederCount; j++)
                {
                    // Pick a random member of the adult population
                    int breeder = rand.Next(0, adultPopulation.Count);

                    // Add it to the breeding Set and remove from the
                    // adult population
                    breedingSet.Add(adultPopulation [breeder]);
                    adultPopulation.Remove(adultPopulation [breeder]);
                }

                // Sort the breeding set by size (descending)
                descendingSort(breedingSet);

                // Breed the adults based on best gets second best
                // until none are left
                while (breedingSet.Count > 1)
                {
                    // Declare the position of the alpha/beta
                    // (or the best and second best breeders)
                    int alphaPos = 0;
                    int betaPos = 1;

                    // Store the entities for breeding
                    entity alpha = breedingSet [alphaPos];
                    entity beta = breedingSet [betaPos];

                    // Generate a number to use for chance
                    // of breeding, percentage is set at run time
                    // so if the number generated is less than that
                    // then its within the percentage. And therefore breeds
                    int breed = rand.Next(0, 100);
                    if (breed <= breedingPercentage)
                    {
                        // Breeding YAY!

                        // How many babies?
                        int offSpring = rand.Next(minOffspring, maxOffspring);
                        for (int j = 0; j < offSpring; j++)
                        {
                            int babySize = 0;
                            // 10% chance of a+b
                            // 10% chance of a+1/2b
                            // 10% chance of 1/2a+b
                            // 20% chance of average(a,b)
                            // 20% chance of a+rand(1/2a, 2a)
                            // 20% chance of b+rand(1/2b, 2b)
                            // 5% chance of 1/2a
                            // 5% chance of 1/2b
                            int sizeChance = rand.Next(0, 100);
                            if (sizeChance <= 10)
                            {
                                babySize = alpha.Size + beta.Size;
                            } else if (sizeChance > 10 && sizeChance <= 20)
                            {
                                babySize = alpha.Size + beta.Size / 2;
                            } else if (sizeChance > 20 && sizeChance <= 30)
                            {
                                babySize = alpha.Size / 2 + beta.Size;
                            } else if (sizeChance > 30 && sizeChance <= 50)
                            {
                                babySize = (alpha.Size + beta.Size) / 2;
                            } else if (sizeChance > 50 && sizeChance <= 70)
                            {
                                babySize = alpha.Size + rand.Next(beta.Size / 2, beta.Size * 2);
                            } else if (sizeChance > 70 && sizeChance <= 90)
                            {
                                babySize = rand.Next(alpha.Size / 2, alpha.Size * 2) + beta.Size;
                            } else if (sizeChance > 90 && sizeChance <= 95)
                            {
                                babySize = alpha.Size / 2;
                            } else if (sizeChance > 95 && sizeChance <= 100)
                            {
                                babySize = beta.Size / 2;
                            }

                            // Create the baby
                            entity baby = new entity(babySize, 0, deathAge);
                            babyPopulation.Add(baby);
                        }
                    }

                    // Add the two that just bred back into
                    // standard adult pop
                    adultPopulation.Add(alpha);
                    breedingSet.Add(beta);
                    // and remove from breeding set
                    breedingSet.Remove(alpha);
                    breedingSet.Remove(beta);

                    // re-order the breeding set
                    descendingSort(breedingSet);
                }


                foreach (entity e in adultPopulation)
                {
                    
                }
                foreach (entity e in babyPopulation)
                {
                    e.Age += 1;
                }

                // Age everyone (adults and babies) up by 1
                foreach (entity e in adultPopulation)
                {
                    e.Age += 1;
                }
                foreach (entity e in babyPopulation)
                {
                    e.Age += 1;
                }

                // Display this generation of adults
                Console.WriteLine("Adults");
                foreach (entity e in adultPopulation)
                {
                    e.writeSize();
                }
                Console.WriteLine("");
                Console.WriteLine("Babies");
                foreach (entity e in babyPopulation)
                {
                    e.writeSize();
                }

                Console.SetCursorPosition(0, Console.WindowHeight - 1);
                Console.ReadKey();
            }

        }

        public static void populateList(int num)
        {
            for (int i = 0; i < num; i++)
            {
                // Generate a random initial size
                int size = rand.Next(minEntitySize, maxEntitySize);
                int div = rand.Next(1, 4);
                // Divide the inital size by either 1/2/3 as decided
                // above, giving a higher chance of low value
                // sizes
                size = size / div;

                // Also have a random starting age from adult to death
                int age = rand.Next(adultAge, deathAge/2);

                // Create an entity with the size from
                // above generation
                entity e = new entity(size, age, deathAge);

                // Add the entity to the list
                adultPopulation.Add(e);
            }
        }

        public static void descendingSort(List<entity> l)
        {
            l.Sort((x, y) => y.Size.CompareTo(x.Size)); 
        }
    }
}
